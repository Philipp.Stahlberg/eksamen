package inf101h22.observable;

/**
 * An ObservableValue is a wrapper around a value which allows someone in
 * possesion of the object to register a method (called an observer) to be
 * called every time the value changes.
 * 
 * <p>
 * This can be useful in a wide variety of settings; for example in the
 * model-view-controller design pattern, where a view can observe updates in the
 * model which that particular view cares about.
 * 
 * <p>
 * Example usage:
 * 
 * <pre>{@code
 * class MyDynamicIntegerLabel extends JLabel {
 * 
 *     private final ObservableValue<Integer> myInteger;
 * 
 *     MyDynamicIntegerLabel(ObservableValue<Integer> myInteger) {
 *         this.myInteger = myInteger;
 *         this.myInteger.addObserver(this::onMyObservedValueChanged);
 *         this.onMyObservedValueChanged();
 *     }
 * 
 *     private void onMyObservedValueChanged() {
 *         int newValue = this.myInteger.getValue();
 *         this.setText("" + newValue);
 *     }
 * }
 * }</pre>
 * 
 * In the above example, the method {@code onMyObservedValueChanged} will be 
 * called every time the value in the observable changes.
 */
public interface ObservableValue<E> extends Observable {
    
    /** Gets the current value. */
    E getValue(); 
}
