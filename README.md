# INF101 Eksamen høsten 2022

For å prøve eksamen uten løsningsforslag:
* [Oppgavetekst](./oppgavetekst.pdf)
* [Startkode](./startkode.zip)
* Oppgavetekst for oppgave 6: [bokmål](./treedrawer-bokmal.md), [nynorsk](./treedrawer-nynorsk.md).

Løsningsforslag
* [Sensorveiledning](./sensorveiledning.pdf)
* For løsningsforslag til kodespørsmål, se også koden i dette repositoriet
